"use strict";

const tape = require('tape')
 	,rLib = require( './index.js' )
 	,rangeSort = function(a,b){return a.s - b.s}
 	;


tape('Creating Calendar and Range', (assert) => {
	assert.plan(1);


	assert.doesNotThrow( ()=> rLib.createRange(20160101,20160110,{ "state":"a"} ) );


});

tape('Range Sort By Start and Duration',(assert) => {
	assert.plan(4);

	let ranges = [
		rLib.createRange( 0,999 )
		,rLib.createRange( 20,25 )
		].sort( rLib.sortByStartAndLength );

	
	assert.equals(ranges[0].t,999);
 	
 	ranges = [
		rLib.createRange( 20,25 )
		,rLib.createRange( 20,200 )
	].sort( rLib.sortByStartAndLength );

	assert.equals(ranges[0].t,180);

	ranges = [
		rLib.createRange( 20,25 )
		,rLib.createRange( 20,200 )
		,rLib.createRange( 21,22 )
	].sort( rLib.sortByStartAndLength );

	assert.equals(ranges[0].t,180);
	assert.equals(ranges[2].t,1);
} );

tape('Range Math Tests', (assert) => { 
	assert.plan( 16 );

	assert.true( rLib.intersects( 20160202, 20160210, 20160205, 20160220 ) );
	assert.true( rLib.intersects( 20160202, 20160210, 20160120, 20160205 ) );
	assert.false(rLib.intersects( 20160202, 20160210, 20160130, 20160201 ) );
	assert.false(rLib.intersects( 20160202, 20160210, 20160211, 20160220 ) );
	assert.false(rLib.intersects( 1488758400000, 1489622400000, 1488326400000, 1488758400000 ) );



	assert.true( rLib.isBetween( 20160205, 20160210, 20160202, 20160230 ) );
	assert.false( rLib.isBetween( 20160201, 20160210, 20160202, 20160230 ) );

	assert.false( rLib.isWithin( 20160205, 20160230, 20160202, 20160220 ) );

	assert.deepEqual(rLib.substract( 
			rLib.createRange(1488758400000, 1489622400000)
			,rLib.createRange(1488326400001, 1488758400000 )
		)
		,
		rLib.createRange(1488758400001,1489622400000 )
		
	);

	assert.deepEqual(rLib.substract( 
			rLib.createRange( 20160202, 20160210, { "state":"n" } ), 
			rLib.createRange( 20160205, 20160220,{ "state":"a" } )
		),rLib.createRange( 20160202,20160204, { "state":"n" })
	);

	assert.deepEqual(rLib.substract( 
			rLib.createRange( 20160202, 20160210,{ "state":"n" } ), 
			rLib.createRange( 20160120, 20160205,{ "state":"a" })
		),rLib.createRange(20160206, 20160210,{ "state":"n" } )
	);

	
	assert.equals(  rLib.substract( 
			rLib.createRange( 20160202, 20160210,{ "state":"n" } ), 
			rLib.createRange( 20160201, 20160220,{ "state":"a" } )
		),null
	);

	assert.deepEqual(  rLib.stamp( 
			rLib.createRange( 20160202, 20160210,{ "state":"n" } ), 
			rLib.createRange( 20160205, 20160220,{ "state":"a" } )
		),[ rLib.createRange( 20160202, 20160204,{ "state":"n" } ) ]
	);



	assert.deepEqual(  rLib.stamp( 
			rLib.createRange( 20, 50 ), 
			rLib.createRange( 20, 40 )
		),[ 
			rLib.createRange( 41, 50 )
		]
	);
	assert.deepEqual(  rLib.stamp( 
			rLib.createRange( 20160202, 20160210,{ "state":"n" } ), 
			rLib.createRange( 20160205, 20160207,{ "state":"a" } )
		),[ 
			rLib.createRange( 20160202, 20160204,{ "state":"n" } ),
			rLib.createRange( 20160208, 20160210,{ "state":"n" } )
		]
	);

	assert.deepEqual(  rLib.stamp( 
			rLib.createRange( 20160202, 20160210,{ "state":"n" } ), 
			rLib.createRange( 20160201, 20160203,{ "state":"a" } )
		),[ 
			rLib.createRange(20160204, 20160210,{ "state":"n" } )
		]
	);


});

tape('Range Flattening',(assert)=>{
	assert.plan(6);
	let ranges = [ 
		rLib.createRange(0,100)
		,rLib.createRange(110,200)
	]

	assert.deepEqual( 
		rLib.flatten( ranges )
		,[
			rLib.createRange(0,100)
			,rLib.createRange(110,200)
		]
	);

	ranges = [ 
		rLib.createRange(0,100)
		,rLib.createRange(90,200)
	];

	assert.deepEqual( 
		rLib.flatten( ranges )
		,[
			rLib.createRange(0,89)
			,rLib.createRange(90,200)
		]
	);

	ranges = [ 
		rLib.createRange(0,100)
		,rLib.createRange(20,40)
	];

	assert.deepEqual( 
		rLib.flatten( ranges )
		,[
			rLib.createRange(0,19)
			,rLib.createRange(20,40)
			,rLib.createRange(41,100)
		]
	);


	ranges = [ 
		rLib.createRange(0,100)
		,rLib.createRange(20,50)
		,rLib.createRange(30,60)
	];

	assert.deepEqual( 
		rLib.flatten( ranges )
		,[
			rLib.createRange(0,19)
			,rLib.createRange(20,29)
			,rLib.createRange(30,60)
			,rLib.createRange(61,100)
		]
	);

	ranges = [ 
		rLib.createRange(0,100)
		,rLib.createRange(20,50)
		,rLib.createRange(20,40)
	];

	assert.deepEqual( 
		rLib.flatten( ranges )
		,[
			rLib.createRange(0,19)
			,rLib.createRange(20,40)
			,rLib.createRange(41,50)
			,rLib.createRange(51,100)
		]
	);


	ranges = [ 
		rLib.createRange(0,200)
			,rLib.createRange(20,90)
				,rLib.createRange(30,50)
				,rLib.createRange(40,60)
				,rLib.createRange(70,80)
			,rLib.createRange(100,120)
		,rLib.createRange(300,400)
	];

	assert.deepEqual( 
		rLib.flatten( ranges )
		,[
			rLib.createRange(0,19)
			,rLib.createRange(20,29)
			,rLib.createRange(30,39)
			,rLib.createRange(40,60)
			,rLib.createRange(61,69)
			,rLib.createRange(70,80)
			,rLib.createRange(81,90)
			,rLib.createRange(91,99)
			,rLib.createRange(100,120)
			,rLib.createRange(121,200)
			,rLib.createRange(300,400)
		]
	);



});

tape('Calendar Setting Ranges', (assert) => { 

	assert.plan(8);

	let cal = [ rLib.createRange(0,9999999999999,{'state':'a'}) ];

	assert.deepEqual( rLib.findAffected( cal, 20160101, 20160110), [cal[0]]  );

	cal = rLib.setRange( cal, rLib.createRange( 20160202, 20160210,{ "state":"n" } ) )
			.sort( rangeSort );


	assert.deepEqual( 
		cal,[
		 rLib.createRange( 0, 20160201, { "state":"a" } )
		,rLib.createRange( 20160202, 20160210, { "state":"n" } )
		,rLib.createRange( 20160211,  9999999999999, { "state":"a" } )
		
	]);


	cal = rLib.setRange( cal, rLib.createRange( 20160501, 20160520,{ "state":"n" } ) )
	 		.sort( rangeSort )	

	assert.deepEqual( cal,[
		 rLib.createRange( 0, 20160201, { "state":"a" } )
		,rLib.createRange( 20160202,  20160210, { "state":"n" } )
		,rLib.createRange( 20160211, 20160500, { "state":"a" } )
		,rLib.createRange( 20160501, 20160520, { "state":"n" } )
		,rLib.createRange( 20160521,  9999999999999, { "state":"a" } )
		
	]);

	//same states inbetween, should only merge
	cal = rLib.setRange( cal, rLib.createRange( 20160601, 20160620,{ "state":"a" } ) )
	 		.sort( rangeSort )	

	assert.deepEqual( cal,[
		 rLib.createRange( 0, 20160201, { "state":"a" } )
		,rLib.createRange( 20160202, 20160210,{ "state":"n" } )
		,rLib.createRange( 20160211, 20160500,  { "state":"a" } )
		,rLib.createRange( 20160501, 20160520, { "state":"n" } )
		,rLib.createRange( 20160521, 9999999999999, { "state":"a" } )
		
	]);

	assert.deepEqual( rLib.findAffected( cal,20160205, 20160310 ),
		[
		rLib.createRange(20160202, 20160210,  { "state":"n" } )
		,rLib.createRange(20160211,  20160500, { "state":"a" } )
		]
	);



		//same states inbetween, should only merge
	cal = rLib.setRange( cal, rLib.createRange( 20160205, 20160310,{ "state":"a" } ) )
	 		.sort( rangeSort )	



	assert.deepEqual( cal,[
		 rLib.createRange( 0, 20160201,  { "state":"a" } )
		,rLib.createRange( 20160202, 20160204,  { "state":"n" } )
		,rLib.createRange( 20160205, 20160500, { "state":"a" } )
		,rLib.createRange( 20160501, 20160520, { "state":"n" } )
		,rLib.createRange( 20160521, 9999999999999, { "state":"a" } )
		
	]);


	assert.deepEqual( rLib.findAffected( cal, 0, 9999999999999),[
		 rLib.createRange(0, 20160201, { "state":"a" } )
		,rLib.createRange( 20160202, 20160204, { "state":"n" } )
		,rLib.createRange( 20160205, 20160500,  { "state":"a" } )
		,rLib.createRange( 20160501, 20160520,  { "state":"n" } )
		,rLib.createRange( 20160521, 9999999999999,  { "state":"a" })
	]);

	cal = rLib.setRange( cal, rLib.createRange( 0, 9999999999999,{ "state":"a" } ) )
		.sort( rangeSort );	

	assert.deepEqual( cal,[
		rLib.createRange( 0,  9999999999999, { "state":"a" } )
	]);

})
