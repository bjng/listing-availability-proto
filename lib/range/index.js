"use strict";


function desc(a,b){return a.s - b.s}

function intersects( s1,e1,s2,e2 ){
	return Math.min( e1,e2 ) - Math.max( s1,s2) > 0;
}

function isBetween(as,ae,bs,be){ //A between B?
	return  bs< as && be > ae;
}

function isWithin(as,ae,bs,be){ //A between B?
	return  as >= bs && ae <= be;
}


function createRange(start,end, extend){
	let r = { "s" : start,"e": end,"t": end -start };
	
	if(extend){
		for(let key in extend){
			if(key === 's' || key === 'e' || key === 't'){continue;}
			r[key] = extend[key];
		}
	}
	return r;
}


// a [ ] [                             ]
// b       [ ] [     ]
// c             [            ]
// d                [            ]
// e                  [    ]
// f                    [     ]
// g                                       []
 
function _flattenFrom( range,index){
	index = index ===undefined ? 0 : index;
	var i = index;

	for (i; i < range.length; i++) {

		let r = range[i]
			,r1 = i+1<range.length ? range[i+1] : null
			,n = i
			,split;

		

		if(r1){
			
			if( r.e < r1.s ){//disconnected
				
				if(index === 0 ){
					continue;
				}
				return i;
			}
			else if( isWithin(r1.s,r1.e, r.s,r.e) > 0 ){
			
				n = _flattenFrom( range,i+1);
				
				split = stamp( r, createRange( r1.s,range[n].e ) );
				
				if(!split){
					range.splice(i,1);
					i--;
				}else{
					if(split[0].s > r.s){
						range.splice( n+1,0,split[0]);
						range.splice( i,1);
					}
					else{
						range.splice( i,1,split[0]);
					}
					
					if(split.length>1){
						range.splice(n+1,0,split[1]);
					}
				}
				i = n;
				continue;
			}
			else{
				
			
		// 				cr.createRange(0,100)
		// ,cr.createRange(20,50)
		// ,cr.createRange(20,40)
				
				split = substract(r,r1);
				if(split){
					range.splice( i,1,split );
				}
				else{
					
					//i--;
				}
				
				continue;
				
				
			}
		}
	};

	return i-1
}

function flatten( range ){
	
	_flattenFrom( range,0);

	return range;
}



function findAffected( calendar, start, end ){
	var result = [];
	for (var i = 0; i < calendar.length; i++) {
		if(calendar[i].state != "d" ){
			if( intersects( start,end,calendar[i].s,calendar[i].e ) ){
				result.push( calendar[i] );
			}
		}
	};
	return result;
}

function sortByStartAndLength( a,b){
	return a.s - b.s != 0 ? a.s - b.s : b.t - a.t;
}


function merge( a,b){
	return createRange( Math.min( a.s,b.s),Math.max( a.e,b.e ),a );
}

// createRange(1488758400000, 1489622400000)
// 		,createRange(1488326400000, 1488758400000 )
		
function substract(a,b){ //a- b

	if( a.s >= b.s && a.e <= b.e ){//erased
		return null;
	}
	else if(a.s < b.s && a.e > b.s){
		
		return createRange( a.s, b.s - 1, a);
	}
	else if( a.s >= b.s && a.e > b.e){

		return createRange( b.e + 1, a.e, a );
	}

	return createRange( a.s, a.e, a );

}



function stamp(a,b){ //b stamps a
	if( a.s >= b.s && a.e <= b.e ){
		return null; 
	}
	else if( isBetween(b.s, b.e, a.s, a.e) ){
		return [
			substract(a,b),
			createRange( b.e + 1, a.e, a)
		]
	}
	else{
		return [substract( a,b )];
	}
		

	
}



//simple object comparison, no arrays 
function equals( a,b ){
	
	if(Object.keys(a).length!=Object.keys(b).length){
		return false;
	}

	for(key in a){
		if(!b[key]){
			return false;
		}
		else if( a[key ] == Object( a[ key ]) ){
			if(! equals(a[key],b[key]) ){
				return false;
			}
		}
		else if(a[key] !== b[key]){
			return false
		}
	}
	
	return true;
	
}

function timeDiffToDay(a,b){
	let msPerDay = 86400000;
	return  Math.round( Math.abs( (a - b)  / msPerDay ) );

}

function setRange( calendar,range){
	var add = []
		,affected = findAffected( calendar, range.s,range.e )
		,a
		;


	for (var i = 0; i < affected.length; i++) {
		a = affected[i];
		if( range.s <= a.s && range.e >= a.e ){
			continue; //swallowed
		}
		if( a.state &&  (a.state === range.state) ){
			range = merge( a, range );
			continue;
		}
		
		add = add.concat( stamp( a, range ) );	
	};

	add.push(range);

	return update(calendar,affected,add)

}

function update(cal,del,add){

	del.forEach( function( range ){
		range.state="d";
	});

	cal = cal.concat( add );

	return cal.filter( function( range ){
		return range.state != 'd';
	});
}



function streamDates( start,end,f ){
	let n = start;
	while( n<=end ){
		f( n );
		n = new Date(n).setDate( new Date(n).getDate() + 1 );
	}
}


module.exports = {
	"isBetween" : isBetween
	,"isWithin" : isWithin
	,"intersects" : intersects
	,"createRange" : createRange
	,"setRange" : setRange
	,"merge": merge
	,"substract" : substract
	,"stamp": stamp
	,"findAffected": findAffected
	,"desc":desc
	,"timeDiffToDay" : timeDiffToDay
	,"sortByStartAndLength": sortByStartAndLength
	,"flatten":flatten
};

