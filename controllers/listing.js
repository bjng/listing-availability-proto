const D = require('../data');





module.exports = {

	"index": function *() {
		//console.log("data", this.hits);
		yield this.render("listing-index", {
			canonical: this.host + this.url,
			title: "Listings",
			hits: this.hits,
			style: this.style
		});
	},
	"single": function*( ){}
}