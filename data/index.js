"use strict"
const elasticsearch = require('elasticsearch');
const C = require('../config');

const client = new elasticsearch.Client({
  host: [C.elasticsearchHost,C.elasticsearchPort].join(':'),
  log: ['info','warning','error']
});


module.exports = {
	"D": client,
	"client": function*( next ){
		this.D = client;
		yield next;
	}
};