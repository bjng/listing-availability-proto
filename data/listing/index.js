"use strict"
const D = require('../index')
  , rangeLib = require('../../lib/range')
  , Lazy = require('lazy.js')
  , thunkify = require('thunkify')
  ;


function isRuledOut( listing ){
  return false;
}



let ruleAggregators = {
  "MinimumDays" : function( l,q,rules ){
    return true;
  }
  ,"CheckInDay" : function( l,q,rules ){ 
    return true;
  }
  ,"Pricing" : function( l,q,rules ){ 

    var totalPrice = 0
      ,qRange = rangeLib.createRange(q.s,q.e)
      ,msPerDay = (1000*60*60*24)
      ;

  
    let agg = Lazy(rangeLib.flatten(rules))
      .reduce( function(x,r){
        
        if(r.s<qRange.s){//cut of left
          r.s = qRange.s;
          r.t = r.e - r.s;
  
        }
        if(r.e > qRange.e){ //cut off right
          r.e = qRange.e;
          r.t = r.e - r.s;
        }
        if(r.t<0){
          return x;
        }

        let days = Math.ceil( r.t/msPerDay);

        x.days += days;
        x.total += r.value * days;

        return x;
      },{"total":0,"days":0});
  
    agg.avg = Math.round(agg.total / agg.days);
  
    
    return agg;
  }
}



/*
  Query gets more than needed documents, because rules might rule out availability. Avoids additional DB queries.
*/
module.exports = function*( next ){


  let q =
    {
     "s": new Date().getTime()
    ,"e": 4070908800000 //something high 2099 or so
    ,"lat":37.9776492
    ,"lon":23.6778891
    }
  ;

  if(this.query["t"]){
    let se = this.query["t"].split(',');
    q.e = new Date( se[se.length-1] + 'T00:00:00' ).getTime();
    q.s = se.length > 1 ?  new Date( se[0] + 'T00:00:00' ).getTime() : q.s;
  }
  if( this.params["geo"] ){
    let geo = this.params["geo"].replace("@",'').split(',');
    q.lat = geo[0];
    q.lon = geo[1];
  }


  let size = this.query["size"] || 20
    ,from = this.query["from"] || 0,
    overhead = size * 2; 


  let dsl = {
    "index": 'ltest',
    "type": 'listing',
    "size": overhead,
    "from": from,
    "body": { 

      "query":{

            "function_score": {
                "filter": {
                  "bool":{
                    "must":[
                      {
                        "has_child": {
                           "type": "availability"
                          ,"filter": { 
                      "bool": {
                        "must": [
                          { 
                            "term":{"state":"a"}
                          },  
                          {
                             "range":{
                              "e": {
                                "gt" : q.s

                              }
                            }
                           },
                           {
                             "range":{
                              "s": {
                                "lt" : q.e
                              }
                            }
                           }
                           
                        ]
                      }
                    }
                  }
                      },
                      {
                  "geo_distance" : {
                            "distance" : "30km",
                            "location" : {
                                "lat" : q.lat,
                                "lon" : q.lon
                            }
                        }
                      }
                    ]
                  }
                },
                "functions": [
                    {
                      "gauss": {
                        "location": { 
                          "origin": { "lat": q.lat, "lon": q.lon },
                          "offset": "5km",
                          "scale":  "20km"
                        }
                      }
                    }
                ]
            }
        }
    }
  }
        

  var prefiltered = yield  this.D.search( dsl );


  let ruledHits = Lazy( prefiltered.hits.hits )
      .filter( hit=>{
        hit.ruleAggr = {};
        let valid = true;
        Lazy( hit._source.rules )
          .filter( r=>{ return  rangeLib.intersects( q.s,q.e, r.s,r.e) > 0 } )
          .groupBy('type')
          .values()
          .each( rulesOfType  =>{
            
            
            let type = rulesOfType[0].type  
              ,aggr = ruleAggregators[ type ]( hit._source,q,rulesOfType )
            ;
      
            if( !aggr ){
              valid = false
            }
            hit.ruleAggr[type] = aggr;
          
          });
        
        
        return valid;

      })
      .toArray();
  
  this.hits = {
    "total": prefiltered.hits.total
    ,"took": prefiltered.took
    ,"hits" : ruledHits
  }

  yield next;

}

