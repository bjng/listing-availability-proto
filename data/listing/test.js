"use strict"

const listing = require('./index.js');

let start = new Date( Date.UTC( new Date().getFullYear() + 1,2,1,0,0,0,0)).getTime()
	,end = new Date( start ).setDate( new Date(start).getDate() + 8 );


listing.getAvailable({
	"s": start, 
	"e": end 
})
.then(re=>console.log(re.total,"in",re.took),er=>console.trace(er));
