const koa = require('koa')
	,C = require('./config')
	,R = require('./routes')
	,D = require('./data')
	,handlebars = require("koa-handlebars")
	,serve = require("koa-static")
	,env = process.env;


var app = module.exports = koa();

app.use(handlebars( C.template ));


app
.use( serve('static/') )
.use( D.client )
.use( R.routes() )
.use( R.allowedMethods() );


app.listen(env.NODE_PORT || 3000, env.NODE_IP || 'localhost');
