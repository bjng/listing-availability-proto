#Cretico - Search for Availability (Proof of Concept)

[Cretico](https://creti.co/) is an AirBnB similar Webpage, which specialises on villas with private pools in Greece.

This prototype was created to solve some bigger problems with their search and managed to

* speed up the search of current queries immensely
* allow in future more flexible queries for availabilities
* reduce the data stored in the DB
* explore different technologies for a future recoding of Cretico

Below is a detailed description of the problem, I am in particular proud of the fact that I managed to do the filtering with only one Query to the DB, solution I found online included always at least two queries, based on unavailability and not availability.

## Technologies

Node.js, [Koa.js](http://koajs.com/), [Lazy.js](http://danieltao.com/lazy.js/), [Elasticsearch](https://www.elastic.co/products/elasticsearch), [Tape for testing](https://github.com/substack/tape), [Openshift 2](https://www.openshift.com/)

## Code Examples

* [Lib for Flattening](https://bitbucket.org/bjng/listing-availability-proto/src/60ff1ecd6ec4874dcf7ffdd24847e50de2294749/lib/range/index.js?at=master&fileviewer=file-view-default)
* [Test Cases for Lib](https://bitbucket.org/bjng/listing-availability-proto/src/60ff1ecd6ec4874dcf7ffdd24847e50de2294749/lib/range/test.js?at=master&fileviewer=file-view-default)
* [Elasticsearch Queries](https://bitbucket.org/bjng/listing-availability-proto/src/12af46753851e6d1c36cf0347f822f62a00e17b8/data/listing/index.js?at=master&fileviewer=file-view-default)

## Details

When I started at Cretico the search for their listings was *incredible* slow and needed urgently refactoring. With some smaller changes, including preaggregation of the listing galleries, the search turned out already 10x faster. Better but still way to slow.

One of the biggest problems was that there was now way to filter the listings by availability without calling the database twice.
The availability was updated from different sources, Creticos own bookings and other synced calendars from AirBnB for example. The data stored actually showed only unavailabilty and not availability, which didn't allow easily fancier queries like "+/- 3 days" or "3 days within".

I thought I could address all these issues if I flatten all the unavailability blocks for a listing to one stream, which would also include the availabilities.

The flattend blocks would show the exact start and end, but also how many days are in that block. Queries would become much easier and faster, plus, the amount of objects stored in the DB would be reduced at the same time.

Creticos code itself is a little complex and sometimes a little messy, based on a bespoke PHP and Mysql Framework.

To proof it first I created this Prototype in **Node.js ([Koa](http://koajs.com/))**and also tried out **Elasticsearch**, because on one point I wanted to rebuild Cretico for this setup anyway. The development was completely **Test Driven** from the beginning via [Tape](https://github.com/substack/tape), which is a really effective and simple testing lib for javascripts. 

The graph below show the principle Idea behind the flattening.

![Concept](https://bytebucket.org/bjng/listing-availability-proto/raw/82e327b37b103d1aae3761522c12d07ba743ba62/docs/flatcalendar.png)