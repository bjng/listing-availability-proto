module.exports = {
	"elasticsearchHost": process.env["OPENSHIFT_ELASTICSEARCH_IP"] || 'localhost',
	"elasticsearchPort": process.env["OPENSHIFT_ELASTICSEARCH_PORT"] || '9200',
	"template":{
	  "defaultLayout": "main",
	  "cache": process.env["NODE_ENV"] === 'production'
	}
}