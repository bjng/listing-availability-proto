const 	
		available = require('../data/listing')
		,style = require('../middleware/style')
		,Listing = require('../controllers/listing')
		,Status = require('../controllers/status');

module.exports = function(R){


	R.get('/h', 
		Status.health 
	);

	R.get('/s/:term?/:geo?',
		available
		,style('listing-index') 
		,Listing.index 
	);

	R.get('/l/:id', 
		Listing.single 
	);


}