const 
	thunkify = require( 'thunkify')
	,fs = require('fs')
	,path = require('path')
	,read = thunkify(fs.readFile);

module.exports = function(name){
	return function*(next){
		this.style = yield read( path.resolve( process.cwd(), 'styles/css/'+name+'.css' ) );
		yield next;
	}
}