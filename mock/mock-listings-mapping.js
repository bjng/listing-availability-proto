"use strict"

const D =  require('../data').D;



console.log('Deleting Test Index');
D.indices.delete({
  "index": 'ltest',
  "ignore": [404],
  "refresh":true
},function (error, response) {
  console.log(response);
  createMapping();
})


function createMapping( body ){

	console.log('Creating Mapping');

	D.indices.create({
		"index": 'ltest',
		"body" : {
			"mappings":{
				"availability":{
					"_parent": {
						"type": "listing" 
					},
					"properties": {
						"s": { 
							"type" : "date"
						},
						"e": { 
							"type" : "date"
						},
						"t":{ 
							"type": "long"
						},
						"state":{
							"type": "string"
						}
					}
				},
				"listing":{
					"_timestamp": { 
						"enabled": true
					},
					"properties": {
						"location": {
							"type": "geo_point"
						},
					
						"amenities":{
							"type":"string"
						},
						"rules": {
							"type": "nested", 
							"properties": {
								"type":    { 
									"type": "string"  
								},
								"s": { 
									"type" : "date"
								},
								"e": { 
									"type" : "date"
								},
								"t":{ 
									"type": "long"
								},
								"value": { 
									"type":"double"
								}
							}
						},
						"gallery" : {
							"type": "nested",
							"properties": {
								"url":{ 
									"type": "string"  
								},
								"caption":{ 
									"type": "string"  
								}
							}
						},
						"user":{
							"type": "nested",
							"properties":{
								"_id": { "type" : "string" },
								"imageUrl": { "type" : "string" }
							}
						},
						"description": { 
							"type": "string"  
						}
					}
				}
			}
		}
	}).then( 
		e=>console.log(e)
	).catch(e=>{
		console.trace(e);
	});
		
}