"use strict"

const D =  require('../data').D
	,md5 = require('md5')
	,cr = require('../lib/range')
	,now = new Date();



function shuffle(o){
	for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
}


function streamRandomRanges( f, maxDays, maxGap, skip, overlap ){
	
	skip = skip === undefined || skip === null ? 0 : skip; //

	if( skip === 1 ){ return [] };


	for (let i = 0; i < years; i++) {
		let year = new Date( (new Date().getFullYear()+i)+'-01-02')
			,nextDayAt = 1
			,start = new Date( year.getTime() )
			,end = new Date( year.getTime())
			;

			while( end.getFullYear()==year.getFullYear()){
				


				start = new Date( year.getTime())
				start.setUTCDate( nextDayAt );
			
				nextDayAt+=  Math.max(2,Math.floor((Math.random() * maxDays ) + 1));

				end = new Date( year.getTime())
				end.setUTCDate( nextDayAt );

				if(Math.random() >= skip ){
					f( start,end );
				}
				
				if(end.getUTCFullYear()!=year.getUTCFullYear() ){
					break;
				}

				nextDayAt+= Math.floor( (Math.random() * maxGap ) + 1);
				if(overlap && Math.random() > overlap){
					Math.floor( (Math.random() * maxGap ) );
				}


			}
	};

};


function createMapping( body ){

	console.log('Creating Mapping');

	D.indices.create({
		"index": 'ltest',
		"body" : {
			"mappings":{
				"availability":{
					"_parent": {
						"type": "listing" 
					},
					"properties": {
						"s": { 
							"type" : "date"
						},
						"e": { 
							"type" : "date"
						},
						"t":{ 
							"type": "long"
						},
						"state":{
							"type": "string"
						}
					}
				},
				"listing":{
					"properties": {
						"location": {
							"type": "geo_point"
						},
						"amenities":{
							"type":"string"
						},
						"rules": {
							"type": "nested", 
							"properties": {
								"type":    { 
									"type": "string"  
								},
								"s": { 
									"type" : "date"
								},
								"e": { 
									"type" : "date"
								},
								"t":{ 
									"type": "long"
								},
								"value": { 
									"type":"double"
								}
							}
						},
						"gallery" : {
							"type": "nested",
							"properties": {
								"url":{ 
									"type": "string"  
								},
								"caption":{ 
									"type": "string"  
								}
							}
						},
						"user":{
							"type": "nested",
							"properties":{
								"_id": { "type" : "string" },
								"imageUrl": { "type" : "string" }
							}
						},
						"description": { 
							"type": "string"  
						}
					}
				}
			}
		}
	}).then( 
		r=>createMock(),
		e=>console.log(e)
	).catch(e=>{
		console.trace(e);
	});
		
}


function bulkInsert( bulk,callback ){
	const bulkSize =100;
	let n = bulk.length;
	let sendBatch = batch=>{
		process.stdout.write(bulk.length + ',');
		D.bulk({
			"body":  batch
		})
		.then( 
			re=>{ 
				if(bulk.length > 0){
				
					sendBatch( bulk.splice(0,bulkSize) );
				}
				else{
					callback(null,n);
				}
			}
			,er=>{ callback(er,null); }
		);
	}

	sendBatch( bulk.splice( 0,bulkSize ) );

}

function createMock(  ){
	console.log('Creating Bulk');

	console.time("Creation Time Listings");
	var bulk = createListingBulk(n);
	console.timeEnd("Creation Time Listings");

	console.time("Bulk Insert Time");

	bulkInsert( bulk,(er,re)=>{
		if(er){console.log(er)}
		else{
			D.indices.refresh({"force":true},function(){
				createAllBookings();
				console.timeEnd("Bulk Insert Time");
			})
			
			
		}
	});


}

const n = 1000
	,bounds= {
		"sw": {"lat": 34.346210,"lon":19.592384}
		,"ne":{"lat": 42.241323,"lon":29.011579}
	}
	,amenities = ["wifi","tv","firstaidkit","wheelchairfriendly","smoking","heating","heatedpool","safe","bbq","internet","pets"]
	,rulesConf = [
		 { "type" : "MinimumDays", "values":[1,5,7,10] }
		,{ "type" : "CheckInDay", "values": [0,1,2,3,4,5,6] }
		,{ "type" : "Pricing", "values": [200,300,150]}
	]
	,maxDaysPerRule = 40
	,maxDistancePerRule = 40

;

function createListingBulk(){
	var bulk = []
		,listing
		,i = 0
		;

	
	while(i<n){
		
		listing = createListing(i);
		bulk.push({ index:  { _index: 'ltest', _type: 'listing' } } );
		bulk.push( listing );
		i++;
	}
	return bulk;

}

function createListing(i){


	let ne = bounds.ne
		,sw = bounds.sw
	;
	let l = {
		"name":md5( (new Date()).getTime().toString() + i.toString() )
		,"location": { 
			"lat": ne.lat + Math.random() * (sw.lat - ne.lat),
			"lon": sw.lon + Math.random() * (ne.lon - sw.lon),
		}
		,"amenities": getRandomAmenities()
		,"rules": getRandomRules()
	};

	return l;
}



function getRandomAmenities(){
	return shuffle( [].concat(amenities) ).slice( 0,Math.round(Math.random() * amenities.length ) );
};

function getRandomRules(){

	//default rules first
	
	let rules = rulesConf.map( conf=>{
		let r = cr.createRange( 0,4070908800000);

			;r.type = conf.type
			;r.value = conf.values[ Math.floor( Math.random() * conf.values.length  ) ]
			;
		return r;

	});
 
	streamRandomRanges( function( start, end){

		let conf = rulesConf[ Math.floor( Math.random() * rulesConf.length  )]
			,r = cr.createRange( start.getTime(),end.getTime(),{
				"type": conf.type
				,"value": conf.values[ Math.floor( Math.random() * conf.values.length  ) ]
			})
			;
		
		rules.push( r );

	},maxDaysPerRule,maxDistancePerRule,0,0.7);

	return rules;
}

const maxDistanceOfNextBooking = 20
,maxDaysPerBooking = 14
,years = 3
,maxPrice = 500
,maxPriceChangesPerYear = 10
;


let rangesCreated = 0;

function createAllBookings(){

	console.log("Create All Bookings");
	console.time("Create All Bookings");

	let count = 0;
	let totalRanges = 0;
	

	D.search({
		"index": 'ltest'
		,"type": 'listing'
		,"scroll": "10s"
		,"size": 10
		,"body": { 
			"filter": {
			    "range": {
			      "_timestamp": { 
			        "gte": now
			      }
			    }
			}
		}
	}, function nextBatch(err,scroll){

		if(err){ throw err; return;}

		let total = scroll.hits.total
			,hits = scroll.hits.hits;

		count+=hits.length;


		let bulk = [];

		for (var i = 0; i < hits.length; i++) {

			bulk = bulk.concat( createCalendarBulk( hits[i]._id ) );
		};
		totalRanges += bulk.length/2;
		D.bulk({"body": bulk } )
			.then( resp =>{
				process.stdout.write(hits.length + ',');
		
				if( count < total ){
					D.scroll({
					  scrollId: scroll._scroll_id,
					  scroll: '10s'
					}, nextBatch);
				}
				else{
					D.indices.refresh();

					console.timeEnd("Create All Bookings");
					console.log( scroll.hits.total,rangesCreated);
				}
			},err=>{
				console.trace(err);
			});
			
	});
	
}








function createCalendarBulk( id ){

	let bulk = [],
		cal = [ cr.createRange(0,4070908800000,{"state":"a"}) ];


	streamRandomRanges( function(start,end){
		rangesCreated++;
		cal = cr.setRange( cal, cr.createRange( start.getTime(),end.getTime(),{"state":"n"}) );
	},maxDaysPerBooking,maxDistanceOfNextBooking);

	cal = cal.sort( cr.desc );

	for (var i = 0; i < cal.length; i++) {

		bulk.push( { "index":  { "_index": 'ltest', "_type": 'availability','_parent' : id } }  );
				
		bulk.push( cal.shift() );
	};


	return bulk;

}


createMock();