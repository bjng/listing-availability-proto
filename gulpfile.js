//helpers
const gulp = require('gulp');
const gutil = require('gulp-util');

//style stuff
const stylus = require('gulp-stylus');
const minifycss = require('gulp-minify-css');
const nib = require('nib');
const rupture = require('rupture');
const jeet = require('jeet');
const autoprefixer = require('autoprefixer-stylus');

gulp.task('stylus', ()=> {
  return gulp.src('./styles/src/*.styl')
    .pipe(stylus(
      {
        compress: true
        ,use:[ 
            rupture()
            ,jeet()
            ,nib()
            ,autoprefixer({browsers: ['last 3 version', '> 5%', 'safari 5', 'ios 6', 'android 4','ie >= 9']})
        ]
        ,'include css': true
      }))
    .on('error', gutil.log)
    .pipe(gulp.dest('./styles/css'))
 

});
